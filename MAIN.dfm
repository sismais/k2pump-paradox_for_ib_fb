�
 TFRMMAIN 0  TPF0TFrmMainFrmMainLeft�Top� BorderIconsbiSystemMenu
biMinimize BorderStylebsSingleCaptionK2Pump for InterbaseClientHeight;ClientWidth(Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTop
WidthUHeightCaptionBDE Alias (Origin)Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TGaugeProgressLeft�TopWidth�HeightProgress   TLabelLabel4Left�Top>WidthHeightCaptionUser:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel3Left	Top2Width@HeightCaptionSelect TablesFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel5Left*Top0Width/HeightCaption
Result logFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel6Left�TopVWidth5HeightCaption	Password:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel2Left�TopWidthqHeightCaptionDatabase (Destination):Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TLabelLabel7Left-Top�Width,HeightCaption
Error log:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  	TComboBox
CBDatabaseLeftTopWidthHeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ItemHeight
ParentFontTabOrder OnChangeCBDatabaseChange  TCheckListBoxLBTableLeftTop@Width� Height� Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ItemHeight
ParentFontTabOrder  TButton	BtnExportLeft� TopWidthnHeightCaptionStartEnabledFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSystem
Font.StylefsBold 
ParentFontTabOrderOnClickBtnExportClick  	TCheckBoxCreateTableCheckBoxLeft� TopeWidth� HeightCaptionCreate table statementsChecked	State	cbCheckedTabOrder  	TCheckBoxchkPumpDataLeft� TopyWidthOHeightCaption	Pump dataChecked	State	cbCheckedTabOrder  	TCheckBox	CheckBox1Left� Top� Width� HeightCaptionCreate index statementsTabOrder  TMemoMemo1Left(Top?Width�HeightjLines.Strings  
ScrollBars
ssVerticalTabOrder  TRadioGroupRadioGroup1Left�Top8Width.Height3Caption	 Dialect 	ItemIndexItems.Strings13 TabOrderOnClickRadioGroup1Click  TEditEdit1LeftTop:WidthvHeightTabOrder	Textsysdba  TEditEdit2LeftTopRWidthvHeightPasswordChar*TabOrder
Text	masterkey  TEditEdit3Left�TopWidthHeightTabOrderTextC:\Temp\DADOS.FDB  	TCheckBox	CheckBox3Left� Top� Width� HeightCaptionUse VARCHAR instead of CHARChecked	State	cbCheckedTabOrder  	TCheckBox	CheckBox4Left� Top� Width� HeightCaptionAll fields are NOT NULLTabOrder  	TCheckBox
AutFillNumLeft� Top� Width� HeightCaption!Auto Fill required numeric with 0Checked	State	cbCheckedTabOrder  	TCheckBox
AutFillNonLeft� Top� Width� HeightCaption'Auto Fill required non numeric with " "Checked	State	cbCheckedTabOrder  	TCheckBox	CheckBox5Left� Top� Width� HeightCaptionCommit every 10000 recordChecked	State	cbCheckedTabOrder  TMemoMemoErrorLogLeft(Top�Width�HeightjLines.Strings  
ScrollBars
ssVerticalTabOrder  TButtonbtnCheckAllLeft� Top@WidthLHeightCaption	Check AllTabOrderOnClickbtnCheckAllClick  TButtonbtnLocDataBaseLeft�TopWidth!HeightCaption...TabOrderOnClickbtnLocDataBaseClick  TMemoMemoInstructionsLeftTop8WidthHeight� Color	clBtnFaceLines.StringsInstructions: "01. Install BDE Administrator 5.01/You need Install BDE (Borland Database Engine) 'Administrador version v5.01 (prefered).0After install, open BDE Administrator (in Start %menu) and create a new STANTARD odbc 0connection for type Paradox connection and set  #the folder with your paradox files. $02. Do you need an created database.+To help you, this application comes with a 0DB_NOTABLES.FDB firebird database file, on open #first time, this file is copied to )DB_NOTABLES_NEW.FDB (use it if you like). -03. If you have problems with PRIMARY KEY in conversion, you can do this:/03.01 After creation of table in new database, ,acess  it with a SGDB Admin (like IBExpert, /FlamminRobin, etc.) and remove the problematic table primary key..03.02 Uncheck "Create table statements", then check :the table in table list, and start convertion to it again; ReadOnly	
ScrollBars
ssVerticalTabOrder  TButton
btnPreviewLeft	TopWidthKHeightHint!Preview Tablea Data before ImportCaptionPreviewParentShowHintShowHint	TabOrderOnClickbtnPreviewClick  TTableTableReadOnly	Left�Top  TIBDatabase	Database1Params.Stringsuser_name=sysdbapassword=masterkey LoginPromptDefaultTransactionTransact	IdleTimer 
SQLDialect
TraceFlags OnDialectDowngradeWarning Database1DialectDowngradeWarningLeft$Top  TIBTransactionTransactActiveDefaultDatabase	Database1DefaultAction
TARollbackAutoStopActionsaNoneLeftDTop  TIBQueryTCamposDatabase	Database1TransactionTransactBufferChunks�CachedUpdatesSQL.Strings#SELECT F.RDB$FIELD_NAME AS NOMBRE, (               D.RDB$FIELD_TYPE AS TIPO,&               F.RDB$NULL_FLAG AS NULO(FROM RDB$RELATION_FIELDS F, RDB$FIELDS D/WHERE (F.RDB$FIELD_SOURCE=D.RDB$FIELD_NAME) AND+               (F.RDB$RELATION_NAME=:TABLA) LeftdTop	ParamDataDataTypeftStringNameTABLA	ParamType	ptUnknown   TIBStringFieldTCamposNOMBRE	FieldNameNOMBRESize�  TSmallintFieldTCamposTIPO	FieldNameTIPO  TSmallintFieldTCamposNULO	FieldNameNULO   TIBQueryQuery1Database	Database1TransactionTransactBufferChunks�CachedUpdatesUniDirectional	Left�Top  TOpenDialogOpenDialog1Left�Top   